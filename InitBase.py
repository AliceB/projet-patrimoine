import psycopg2
import sys
from os import path
import requests as rq
import re

def connexion():
    conn = psycopg2.connect("dbname=sim_patrimoine host=localhost user=aliceb password=pr1ncip3 port=5432")
    return conn


def creation_tables(curs):
    tables = []
    tables.append("""
                    CREATE TABLE IF NOT EXISTS periode (
                        id SERIAL PRIMARY KEY,
                        libelle varchar(100)
                    );
                """)
    tables.append("""
                    CREATE TABLE IF NOT EXISTS point (
                        id SERIAL PRIMARY KEY,
                        idtf integer,
                        titre varchar(151),
                        id_periode integer REFERENCES periode(id),
                        adresse varchar(152),
                        fr_titre varchar(153),
                        fr_corps varchar(8500),
                        en_titre varchar(154),
                        en_corps varchar(8500),
                        biblio varchar(1000),
                        site_internet varchar(150),
                        credit varchar(200),
                        latitude float,
                        longitude float
                    )
                """)
    tables.append("""
                    CREATE TABLE IF NOT EXISTS types (
                        id SERIAL PRIMARY KEY,
                        libelle varchar(101)
                    )
                """)
    tables.append("""
                    CREATE TABLE IF NOT EXISTS thematique (
                        id SERIAL PRIMARY KEY,
                        libelle varchar(100),
                        couleur varchar(10),
                        icone varchar(30)
                    )
                """)
    tables.append("""
                    CREATE TABLE IF NOT EXISTS type_point (
                        id_type integer REFERENCES types (id),
                        id_point integer REFERENCES point (id),
                        CONSTRAINT PK_type_point PRIMARY KEY (id_type, id_point)
                    )
                """)
    tables.append("""
                    CREATE TABLE IF NOT EXISTS theme_point (
                        id_theme integer REFERENCES thematique (id),
                        id_point integer REFERENCES point (id),
                        CONSTRAINT PK_theme_point PRIMARY KEY (id_theme, id_point)
                    )
                """)
    tables.append("""
                    CREATE TABLE IF NOT EXISTS point_brut (
                        id SERIAL PRIMARY KEY,
                        idtf integer,
                        titre varchar(151),
                        adresse varchar(300),
                        fr_txt varchar(8650),
                        en_txt varchar(8650),
                        biblio varchar(1000),
                        site_internet varchar(150),
                        credit varchar(200),
                        latitude float,
                        longitude float,
                        periode varchar(100),
                        types varchar(300),
                        thematique varchar(300)
                    )
                """)
    
    for query in tables:
        curs.execute(query)


if __name__ == "__main__":
    #connexion
    
    conn = None
    try:
        conn = connexion()
        curs = conn.cursor()

        #creation des tables
        creation_tables(curs)
        conn.commit()
        #recuperation du JSON
        r = rq.get("http://entrepot.metropolegrenoble.fr/opendata/38185-GRE/Patrimoine/json/PATRIMOINE_VDG_EPSG4326.json")
        if r.status_code == 200:
            data = r.json()["features"]
            lthemes  = []
            ltypes   = []
            lperiode = []

            retype = re.compile(r"((\w+(, )? )*(\((.?)*?\))?(\w+)?)(, )?")

            #print(data)
            for point in data:
                # geometry/coordinates
                coordonnees = point["geometry"]["coordinates"]

                latitude  = coordonnees[1]
                longitude = coordonnees[0]

                # properties
                proprietes = point["properties"]

                idtf     = int(proprietes["idtf"])
                titre    = proprietes["titre"]

                adresse_brut = proprietes["adresse"]["libelle"]
                adresses = re.split(r"\r\n|\r|\n", proprietes["adresse"]["libelle"])

                adrout  = ""
                for adr in adresses:
                    adr = adr.strip()
                    if adr.endswith("Grenoble"):
                        adr = adr[:-8]
                        if adr.endswith((", ",". ")):
                            adr = adr[:-2]
                        if not adr.startswith("Géolocalisation"):
                            adrout += adr + "\r\n"
                if adrout is not "":
                    adrout = adrout[:-2].strip()

                # texte
                texte = proprietes["texte"]

                txt_fr = texte["txt_fr"]
                txt_fr_brut = texte["txt_fr"]
                hastitre = txt_fr.find(r"<h3>")+1
                titre_fr = ""

                if hastitre:
                    fintitre = txt_fr.find(r"</h3>")
                    titre_fr = txt_fr[:fintitre].replace(r"<h3>","").strip()
                    txt_fr = txt_fr[fintitre+5:]

                hasparagraph = txt_fr.find(r"<p>")+1

                if hasparagraph:
                    txt_fr = txt_fr.replace("<\p><p>","\r\n").replace("<p>","").replace("</p>","").strip()

                txt_en = texte["txt_en"]
                txt_en_brut = texte["txt_en"]

                hastitre = txt_en.find("<h3>")+1

                titre_en = ""

                if hastitre:
                    fintitre = txt_en.find("</h3>")
                    titre_en = txt_en[:fintitre].replace("<h3>","").strip()
                    #print("here",hastitre)

                hasparagraph = txt_en.find("<p>")+1

                if hasparagraph:
                    txt_en = txt_en.replace("<\p><p>","\r\n").replace("<p>","").replace("</p>","").strip()

                biblio = texte["bibliographie"].strip()
                site_internet = texte["site_internet"].strip()
                credit = texte["credit"].strip()

                # periode
                periode = proprietes["periode"].strip()
                periode_brut = periode
                #print(periode)
                if periode not in lperiode:
                    lperiode.append(periode)
                    sql = """
                            INSERT INTO periode (libelle)
                            VALUES ( %s )
                        """
                    curs.execute(sql, (periode,))
                    conn.commit()

                sql = """
                        SELECT id FROM periode 
                        WHERE libelle = %s
                        """
                curs.execute(sql, (periode,))
                id_periode = curs.fetchall()[0][0]


                # insertion
                sql = """INSERT INTO point (
                        idtf, titre, id_periode, adresse, fr_titre, fr_corps, en_titre, en_corps, 
                        biblio, site_internet, credit, latitude, longitude
                    )
                    VALUES (
                        %s, %s, %s, %s, %s, %s, %s, %s,
                        %s, %s, %s, %s, %s
                    )
                    RETURNING id
                    """
                curs.execute(
                    sql,(
                        idtf, titre, id_periode, adrout, titre_fr, txt_fr, titre_en, txt_en, 
                        biblio, site_internet, credit, latitude, longitude
                        )
                    )

                id_point = curs.fetchone()[0]
                conn.commit()
                # recup index
                

                themes_brut = proprietes["thematiques"]
                thematiques = proprietes["thematiques"].split(", ")
                for theme in thematiques:
                    theme = theme.strip()
                    if theme not in lthemes:
                        lthemes.append(theme)
                        sql = """
                                INSERT INTO thematique (libelle)
                                VALUES (%s)
                            """
                        curs.execute(sql,(theme,))
                        conn.commit()
                    # récupérer l'id pour correspondance
                    sql = """
                            SELECT id FROM thematique
                            WHERE libelle = %s
                        """
                    curs.execute(sql,[theme])
                    id_theme = curs.fetchall()[0][0]
                    #insert
                    sql = """
                            INSERT INTO theme_point (id_theme, id_point)
                            VALUES (%s, %s)
                        """
                    curs.execute(sql, (id_theme,id_point))
                    conn.commit()

                types_brut = proprietes["types"]
                
                tuples = re.findall(retype,types_brut)
                types = []
                for ituple in tuples:
                    if type(ituple) is str:
                        types.append(ituple)
                    else:
                        types.append(ituple[0])

                for cat in types:
                    cat = cat.strip()
                    if not cat == "":
                        if cat not in ltypes:
                            ltypes.append(cat)
                            sql = """
                                    INSERT INTO types (libelle)
                                    VALUES (%s)
                                    RETURNING id
                                """
                            curs.execute(sql, [cat])
                            conn.commit()
                        # récupérer l'id pour correspondance
                        sql = """
                                SELECT id FROM types 
                                WHERE libelle = %s
                            """
                        curs.execute(sql,[cat])
                        id_type = curs.fetchall()[0][0]

                        sql = """
                                INSERT INTO type_point (id_type, id_point)
                                VALUES (%s,%s)
                            """
                        curs.execute(sql,(id_type,id_point))
                        conn.commit()

                sql = """
                        INSERT INTO point_brut 
                        (idtf, titre, adresse, fr_txt, en_txt,
                        biblio, site_internet, credit, latitude, longitude, 
                        periode, types, thematique)
                        VALUES
                        (%s, %s, %s, %s, %s, 
                         %s, %s, %s, %s, %s,     
                         %s, %s, %s)
                    """
                curs.execute(sql,(
                    idtf,titre, adresse_brut, txt_fr_brut, txt_en_brut,
                    biblio, site_internet, credit, latitude, longitude,
                    periode_brut, types_brut, themes_brut
                ))
                conn.commit()

                sql = """
                        UPDATE thematique
                        SET couleur = 'blue',
                        icone = 'tower'
                        WHERE id = 1
                    """
                curs.execute(sql)
                sql = """
                        UPDATE thematique
                        SET couleur = 'red',
                        icone = 'flag'
                        WHERE id = 2
                    """
                curs.execute(sql)
                sql = """
                        UPDATE thematique
                        SET couleur = 'green',
                        icone = 'tree-deciduous'
                        WHERE id = 3
                    """
                curs.execute(sql)
                sql = """
                        UPDATE thematique
                        SET couleur = 'orange',
                        icone = 'stats'
                        WHERE id = 4
                    """
                curs.execute(sql)
                sql = """
                        UPDATE thematique
                        SET couleur = 'purple',
                        icone = 'picture'
                        WHERE id = 5
                    """
                curs.execute(sql)
                sql = """
                        UPDATE thematique
                        SET couleur = 'lightblue',
                        icone = 'camera'
                        WHERE id = 6
                    """
                curs.execute(sql)
                conn.commit()

        else:
            print("Impossible de récupérer les données", file=sys.stderr)

        #parsing et insertions
    except psycopg2.DatabaseError as e:
        print(e,file=sys.stderr)
    finally:
        if conn != None:
            conn.close()